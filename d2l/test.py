import torch

print(torch.cuda.is_available())
print(torch.cuda.get_device_capability())

print(torch.cuda.get_device_name())

m1 = torch.arange(20).reshape((4,5))

m1

m1.sum(dim=1,keepdim=True)

t = ([[1,2], [3,4]],['a', 'b'])

t

y = torch.tensor([0, 2])
y_hat = torch.tensor([[0.1, 0.3, 0.6], [0.3, 0.2, 0.5]])
y_hat[[0, 1], y]


m1[[0,2],[0,4]]

l1 = [[ 0,  1,  2,  3,  4],
        [ 5,  6,  7,  8,  9],
        [10, 11, 12, 13, 14],
        [15, 16, 17, 18, 19]]



a = torch.tensor(1, dtype=torch.int32)
b = torch.tensor(1.0, dtype=torch.float32)

a == b

m1.shape

m1.argmax()

m1.argmax(axis=0)

m1

m1.argmax(axis=1,keepdim=True)

m2 = torch.normal(0,1, size=(4,5))

m2

m2.argmax(axis=1)

m3 = torch.tensor([ [1],
                    [2],
                    [3]])

m3.shape

m4 = torch.tensor([1,2,3])

m4

m3 == m4

m3.shape

m4.shape

m5 = torch.tensor([0,1,1])
m6 =torch.tensor([[0], [1], [1]])

m5, m6

m5.shape, m6.shape

m5 == m6

a = torch.Tensor(3,4).uniform_(-1,1)

b = torch.zeros_like(a)

a

torch.max(a, b)

torch.maximum(a, b)


